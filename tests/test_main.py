import unittest
from unittest.mock import patch

import virden_jobs.common.web_content
import virden_jobs.common.job
import vjobs_ebrandon.main as vjobs_ebrandon


class WebContentTestCase(unittest.TestCase):

    @patch('requests.get')
    def test_get_jobs_should_return_list(self, mock):
        mock.return_value = mock
        mock.content = '''
<html><body>
<div class="large-6 medium-7 columns"><a href="https://job">job</a></div>
<div class="test2"><a href="https://job2">job2</a></div>
<div class="large-6 medium-7 columns"><a href="https://job3">job3</a></div>
</body></html>
'''

        raw_web_content = virden_jobs.common.web_content.RawWebContent()
        raw_web_content.populate('https://site')
        jobs = vjobs_ebrandon._get_jobs(raw_web_content)

        job = virden_jobs.common.job.Job('job', 'https://job')
        job2 = virden_jobs.common.job.Job('job3', 'https://job3')

        self.assertListEqual(jobs, [job, job2])


if __name__ == '__main__':
    unittest.main()
